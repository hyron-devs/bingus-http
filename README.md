# bingus-http

concurrent http framework in rust

this project is now in maintenance-only mode
# Examples

run `cargo run -r --example hello_world` to run the [hello world example](/examples/hello_world.rs)

you can find the other (cooler) examples in the [examples](/examples) directory

# Features

- uhh its a http server
- it has route parameters and stuff
- it lets you pass a state to your handlers
- it has a `cool_macro!`
- its tiny (<1000 SLOCs)
- i think its kinda fast
- it has a `cool_macro!`
- good for simple apps

# Limitations

- no planned middleware support
- i did not fuzz this
- idk probably bad compared to axum

# Contributing

I don't know why you would want to contribute to this, but if you want to do so,
you can submit a merge request.

# Credits

- [tide](https://github.com/http-rs/tide), for the unholy handler trait

# License

This project is licensed under [The MIT License](https://opensource.org/license/mit/).
See the [LICENSE](/LICENSE) file for more details.
