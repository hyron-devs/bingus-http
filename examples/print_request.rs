extern crate bingus_http;

use std::env;

use anyhow::Result;
use bingus_http::{cool_macro, App, Request};

async fn debug(request: Request<()>) -> Result<String> {
    Ok(format!("{:#?}\n", request))
}

#[tokio::main]
async fn main() {
    env_logger::builder()
        .filter_level(log::LevelFilter::Info)
        .parse_default_env()
        .init();

    let host = env::var("HOST").unwrap_or("0.0.0.0".to_string());
    let port: u16 = env::var("PORT").unwrap_or_default().parse().unwrap_or(8000);

    App::new(())
        .add_handler(cool_macro!(GET /:var), debug)
        .add_handler(cool_macro!(GET /:var1/:var2), debug)
        .add_handler(cool_macro!(GET / *), debug)
        .listen((host, port))
        .await
        .unwrap();
}
