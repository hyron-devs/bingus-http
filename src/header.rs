use std::{
    collections::HashMap,
    fmt::{Debug, Display},
    hash::{Hash, Hasher},
};

#[derive(PartialEq, Eq)]
pub struct HeaderName(String);

impl From<String> for HeaderName {
    #[inline]
    fn from(string: String) -> Self {
        Self(string.to_lowercase())
    }
}

impl<'a> From<&'a str> for HeaderName {
    #[inline]
    fn from(str: &'a str) -> Self {
        Self::from(str.to_string())
    }
}

impl From<HeaderName> for String {
    #[inline]
    fn from(header: HeaderName) -> Self {
        header.0.to_string()
    }
}

impl Hash for HeaderName {
    #[inline]
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.hash(state);
    }
}

impl Debug for HeaderName {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(&self.0, f)
    }
}

impl Display for HeaderName {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self.0, f)
    }
}

pub type Headers = HashMap<HeaderName, String>;
